# EPAM 202.2 Blueprint Advanced Unser Interface Creation

Developed with Unreal Engine 5

## Task Description: 

Create a new Game Level where you need to create a UMG Widget on startup. Widgets appear on the level with some animation: for instance, slowly moving from the bottom to its defined position.

Widget should have at least one button: Pressing a button changes text on the button and change the background picture of the widget. Additionally pressing a button should trigger a music play which should be added as Content to the project. 

## Acceptance criteria: 

1. Able to open a Project in UE5 
2. Able to open and run a Game level 
3. Game level should construct a UMG widget on startup – Level itself should look empty when you open it. 
4. Able to press a button and notice music play and background image change. 
5. Pressing a button second time – stops music play and switches widget background. 

![Result.](/readme/result.PNG "Result.")

![binding.](/readme/binding.PNG "binding.")

![lvl.](/readme/lvl.PNG "lvl.")

![widget.](/readme/widget.PNG "widget.")